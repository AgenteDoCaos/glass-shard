window.onload = function(){
    var LEFT = 37, UP = 38, RIGHT = 39, DOWN = 40;

    var cnv = document.querySelector("canvas");
    var ctx = cnv.getContext("2d");

    var spritesheet = new Image();
    spritesheet = "../imagens/ray.png";

    var ray = new Sprite(spritesheet);

    window.addEventListener("keydown", keydownHandler, false);
    function keydownHandler(e){
        switch(e.keyCode){
            case RIGHT:
                ray.mvRight = true;
                ray.mvLeft = false;
                ray.mvUp = false;
                ray.mvDown = false;
                break

            case LEFT:
                ray.mvRight = false;
                ray.mvLeft = true;
                ray.mvUp = false;
                ray.mvDown = false;  
                break    

            case UP:
                ray.mvRight = false;
                ray.mvLeft = false;
                ray.mvUp = true;
                ray.mvDown = false;  
                break 

            case DOWN:
                ray.mvRight = false;
                ray.mvLeft = false;
                ray.mvUp = false;
                ray.mvDown = true;  
                break                   
        }
    }

    function keyupHandler(e){
        switch(e.keyCode){
            case RIGHT:
                ray.mvRight = false;
                break
            case LEFT:
                ray.mvLeft = false;
                break  
            case UP:
                ray.mvUp = false;
                break    
            case DOWN:
                ray.mvDown = false;
                break                  
        }

    }

     spritesheet.onload = function(){
        init();
       }

    function update(){
      ray.move();
    }

    function init(){
       loop();
    }

    function draw(){
        ctx.clearRect(0,0,cnv.width, cnv.height);
        ray.draw();
    }

    function loop(){
        window.requestAnimationFrame(loop, cnv);
        update();
        draw();
    }
}