function Sprite(img){
//atributos
   this.srcX = this.srcY = 0;
   this.width = 283;
   this.height = 440;
   this.posX = posY = 0;
   this.img = img;
   this.speed = 1;
   this.countAnim =0;

   this.mvRight = this.mvLeft = this.mvUp = this.mvDown = false;

//métodos  

   this.draw = function(ctx){
   ctx.drawImage(this.img, this.srcX, this.srcY, this.width, this.height, this.posX, this.posY, this.width, this.height);
   this.animation();
}

   this.move = function(){
      if(this.mvRight){
         this.posX += this.speed;
         this.srcY = this.height = 2;
      }else

      if(this.mvLeft){
         this.posX -= this.speed;
         this.srcY = this.height = 1;
      }

      if(this.mvUp){
         this.posY -= this.speed;
         this.srcY = this.height = 3;
      }

      if(this.mvDown){
         this.posY += this.speed;
         this.srcY = this.height = 0;
      }
   }

   this.animation = function(){
      if(this.mvLeft || this.mvRight || this.mvUp || this.mvDown){
         this.countAnim++;

          if(this.countAnim >= 12){
            this.countAnim = 0;
          }

         this.srcX = Math.floor(this.countAnim / 4)*this.width;
      }
   }

}